#!/usr/bin/env bash
# Description: GUI for pdftk
# Authores:
#
# License: GNU General Public License v3.0
#
# Dependencies: pdftk, zenity, evince

function menu() {  # Menu principal
    while true; do # Loop infinito
        # Lista de opções do menu principal

        # Opções:
        # --width=600: Largura da janela
        # --height=400: Altura da janela
        # --list: Lista de opções
        # --title " GUI para pdftk ": Título da janela
        # --text " Escolha uma opção: ": Texto
        # --column " Opções ": Coluna de opções

        opcao="$(
            zenity --list --title " GUI para pdftk " \
                --width=600 --height=400 --text " Escolha uma opção: " \
                --column " Opções " \
                " Unir arquivos PDF " \
                " Extrair páginas de um arquivo PDF " \
                " Dividir um arquivo PDF em vários arquivos " \
                " Excluir páginas de um arquivo PDF " \
                " Extrair imagens de um arquivo PDF " \
                " Rotacionar páginas de um arquivo PDF "
        )"

        # Verifica se o usuário clicou em Cancelar ou fechou a janela (exemplo: $opcao = "")
        if [ -z "$opcao" ]; then
            exit 0
        fi

        # Verifica a opção selecionada pelo usuário e chama a função correspondente (exemplo: $opcao = " Unir arquivos PDF ")
        case "$opcao" in
        " Unir arquivos PDF ")
            unir # Chama a função unir
            ;;
        " Extrair páginas de um arquivo PDF ")
            extrair # Chama a função extrair
            ;;
        " Dividir um arquivo PDF em vários arquivos ")
            dividir # Chama a função dividir
            ;;
        " Excluir páginas de um arquivo PDF ")
            excluir # Chama a função excluir
            ;;
        " Extrair imagens de um arquivo PDF ")
            imagens # Chama a função imagens
            ;;
        " Rotacionar páginas de um arquivo PDF ")
            rotacionar # Chama a função rotacionar
            ;;
        *)
            exit 0 # Sai do script
            ;;
        esac # Fim do case
    done     # Fim do loop
}

# Funções ---------------------------------------------------------------------

function unir() { # Unir arquivos PDF

    # Seleciona os arquivos PDF a serem unidos (separados por espaço)
    # Exemplo: /home/user/arquivo1.pdf /home/user/arquivo2.pdf
    # Obs.: O usuário pode selecionar mais de um arquivo

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --file-selection: Selecionar arquivo
    # --multiple: Selecionar mais de um arquivo
    # --separator=" ": Separador de arquivos
    # --title " Selecione os arquivos PDF ": Título da janela
    # --file-filter=" PDF | *.pdf ": Filtro de arquivos

    arquivos=$(
        zenity --width=600 --height=400 \
            --file-selection --multiple --separator=" " \
            --title " Selecione os arquivos PDF " \
            --file-filter=" PDF | *.pdf "
    )

    # Nome do arquivo de saída (exemplo: uniao.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Nome do arquivo de saída ": Título da janela
    # --text " Digite o nome do arquivo de saída: ": Texto
    # --entry-text " uniao.pdf ": Texto padrão

    saida=$(
        zenity --width=600 --height=400 \
            --entry --title " Nome do arquivo de saída " \
            --text " Digite o nome do arquivo de saída: " \
            --entry-text " uniao.pdf "
    )

    # Comando pdftk para unir arquivos PDF (exemplo: pdftk arquivo1.pdf arquivo2.pdf cat output uniao.pdf)
    pdftk $arquivos cat output $saida &>/dev/null # Unir arquivos
    error " Erro ao unir arquivos. "              # Se o último comando não retornar 0 (erro)
    success " Arquivos unidos com sucesso. "      # Se o último comando retornar 0 (sucesso)

    # Visualizar arquivo PDF (exemplo: evince uniao.pdf)
    viewPDF $saida # Visualizar arquivo PDF

}

function extrair() { # Extrair páginas de um arquivo PDF

    # Selecionar arquivo PDF a ser extraído as páginas (exemplo: /home/user/arquivo.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --file-selection: Selecionar arquivo
    # --title " Selecione o arquivo PDF ": Título da janela
    # --file-filter=" PDF | *.pdf ": Filtro de arquivos

    arquivo=$(
        zenity --width=600 --height=400 \
            --file-selection --title " Selecione o arquivo PDF " \
            --file-filter=" PDF | *.pdf "
    )

    # Páginas a serem extraídas (exemplo: 1-5,7,9)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Páginas ": Título da janela
    # --text " Digite as páginas a serem extraídas: ": Texto
    # --entry-text " 1-5,7,9 ": Texto padrão

    paginas=$(
        zenity --width=600 --height=400 \
            --entry --title " Páginas " \
            --text " Digite as páginas a serem extraídas: " \
            --entry-text " 1-5,7,9 "
    )

    # Nome do arquivo de saída (exemplo: extracao.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Nome do arquivo de saída ": Título da janela
    # --text " Digite o nome do arquivo de saída: ": Texto
    # --entry-text " extracao.pdf ": Texto padrão

    saida=$(
        zenity --width=600 --height=400 \
            --entry --title " Nome do arquivo de saída " \
            --text " Digite o nome do arquivo de saída: " \
            --entry-text " extracao.pdf "
    )

    # Extrair páginas do arquivo PDF (exemplo: pdftk arquivo.pdf cat 1-5,7,9 output extracao.pdf)
    pdftk $arquivo cat $paginas output $saida &>/dev/null # Extrair páginas
    error " Erro ao extrair páginas. "                    # Se o último comando não retornar 0 (erro)
    success " Páginas extraídas com sucesso. "            # Se o último comando retornar 0 (sucesso)

    # Visualizar arquivo PDF (exemplo: evince extracao.pdf)
    viewPDF $saida # Visualizar arquivo PDF
}

function imagens() { # Extrair imagens de um arquivo PDF

    # Arquivo PDF a ser extraído as imagens (exemplo: /home/user/arquivo.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --file-selection: Selecionar arquivo
    # --title " Selecione o arquivo PDF ": Título da janela
    # --file-filter=" PDF | *.pdf ": Filtro de arquivos

    arquivo=$(
        zenity --width=600 --height=400 \
            --file-selection --title " Selecione o arquivo PDF " \
            --file-filter=" PDF | *.pdf "
    )

    # Nome do arquivo de saída (exemplo: imagens.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Nome do arquivo de saída ": Título da janela
    # --text " Digite o nome do arquivo de saída: ": Texto
    # --entry-text " imagens.pdf ": Texto padrão

    saida=$(
        zenity --width=600 --height=400 \
            --entry --title " Nome do arquivo de saída " \
            --text " Digite o nome do arquivo de saída: " \
            --entry-text " imagens.pdf "
    )

    # Extrair imagens do arquivo PDF (exemplo: pdftk arquivo.pdf output imagens.pdf uncompress)
    pdftk $arquivo output $saida uncompress &>/dev/null # Extrair imagens
    error " Erro ao extrair imagens. "                  # Se o último comando não retornar 0 (erro)
    success " Imagens extraídas com sucesso. "          # Se o último comando retornar 0 (sucesso)

    # Visualizar arquivo PDF (exemplo: evince imagens.pdf)
    viewPDF $saida # Visualizar arquivo PDF
}

function dividir() { # Dividir um arquivo PDF em vários arquivos

    # Arquivo PDF a ser dividido (exemplo: /home/user/arquivo.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --file-selection: Selecionar arquivo
    # --title " Selecione o arquivo PDF ": Título da janela
    # --file-filter=" PDF | *.pdf ": Filtro de arquivos

    arquivo=$(
        zenity --width=600 --height=400 \
            --file-selection --title " Selecione o arquivo PDF " \
            --file-filter=" PDF | *.pdf "
    )

    # Dividir arquivo PDF (exemplo: pdftk arquivo.pdf burst)
    pdftk $arquivo burst &>/dev/null          # Dividir arquivo
    error " Erro ao dividir arquivo. "        # Se o último comando não retornar 0 (erro)
    success " Arquivo dividido com sucesso. " # Se o último comando retornar 0 (sucesso)

    # Visualizar arquivo PDF (exemplo: evince pg_0001.pdf)
    viewPDF pg_000*.pdf # Visualizar arquivo PDF
}

function excluir() { # Excluir páginas de um arquivo PDF

    # Arquivo PDF a ser excluído as páginas (exemplo: /home/user/arquivo.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --file-selection: Selecionar arquivo
    # --title " Selecione o arquivo PDF ": Título da janela
    # --file-filter=" PDF | *.pdf ": Filtro de arquivos

    arquivo=$(
        zenity --width=600 --height=400 \
            --file-selection --title " Selecione o arquivo PDF " \
            --file-filter=" PDF | *.pdf "
    )

    # Páginas a serem excluídas (exemplo: 1-5,7,9)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Páginas ": Título da janela
    # --text " Digite as páginas a serem excluídas: ": Texto
    # --entry-text " 1-5,7,9 ": Texto padrão

    paginas=$(
        zenity --width=600 --height=400 \
            --entry --title " Páginas " \
            --text " Digite as páginas a serem excluídas: " \
            --entry-text " 1-5,7,9 "
    )

    # Nome do arquivo de saída (exemplo: exclusao.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Nome do arquivo de saída ": Título da janela
    # --text " Digite o nome do arquivo de saída: ": Texto
    # --entry-text " exclusao.pdf ": Texto padrão

    saida=$(
        zenity --width=600 --height=400 \
            --entry --title " Nome do arquivo de saída " \
            --text " Digite o nome do arquivo de saída: " \
            --entry-text " exclusao.pdf "
    )

    # Excluir páginas do arquivo PDF (exemplo: pdftk arquivo.pdf cat 1-5,7,9 output exclusao.pdf)
    pdftk $arquivo cat $paginas output $saida &>/dev/null # Excluir páginas
    error " Erro ao excluir páginas. "                    # Se o último comando não retornar 0 (erro)
    success " Páginas excluídas com sucesso. "            # Se o último comando retornar 0 (sucesso)

    # Visualizar arquivo PDF (exemplo: evince exclusao.pdf)
    viewPDF $saida # Visualizar arquivo PDF
}

function rotacionar() { # Rotacionar páginas de um arquivo PDF

    # Arquivo PDF a ser rotacionado (exemplo: /home/user/arquivo.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --file-selection: Selecionar arquivo
    # --title " Selecione o arquivo PDF ": Título da janela
    # --file-filter=" PDF | *.pdf ": Filtro de arquivos

    arquivo=$(
        zenity --width=600 --height=400 \
            --file-selection --title " Selecione o arquivo PDF " \
            --file-filter=" PDF | *.pdf "
    )

    # Páginas a serem rotacionadas (exemplo: 1-5,7,9)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Páginas ": Título da janela
    # --text " Digite as páginas a serem rotacionadas: ": Texto
    # --entry-text " 1-5,7,9 ": Texto padrão

    paginas=$(
        zenity --width=600 --height=400 \
            --entry --title " Páginas " \
            --text " Digite as páginas a serem rotacionadas: " \
            --entry-text " 1-5,7,9 "
    )

    # Ângulo de rotação (exemplo: 90)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Ângulo ": Título da janela
    # --text " Digite o ângulo de rotação: ": Texto
    # --entry-text " 90 ": Texto padrão

    angulo=$(
        zenity --width=600 --height=400 \
            --entry --title " Ângulo " \
            --text " Digite o ângulo de rotação: " \
            --entry-text " 90 "
    )

    # Nome do arquivo de saída (exemplo: rotacao.pdf)

    # Opções:
    # --width=600: Largura da janela
    # --height=400: Altura da janela
    # --entry: Entrada de texto
    # --title " Nome do arquivo de saída ": Título da janela
    # --text " Digite o nome do arquivo de saída: ": Texto
    # --entry-text " rotacao.pdf ": Texto padrão

    saida=$(
        zenity --width=600 --height=400 \
            --entry --title " Nome do arquivo de saída " \
            --text " Digite o nome do arquivo de saída: " \
            --entry-text " rotacao.pdf "
    )

    # Rotacionar páginas do arquivo PDF (exemplo: pdftk arquivo.pdf cat 1-5,7,9 east output rotacao.pdf)
    pdftk $arquivo cat $paginas east output $saida &>/dev/null # Rotacionar páginas
    error " Erro ao rotacionar páginas. "                      # Se o último comando não retornar 0 (erro)
    success " Páginas rotacionadas com sucesso. "              # Se o último comando retornar 0 (sucesso)

    # Visualizar arquivo PDF (exemplo: evince rotacao.pdf)
    viewPDF $saida # Visualizar arquivo PDF
}

# Funções auxiliares ---------------------------------------------------------

function error() {                   # $1 = Mensagem de erro
    if [ $? -ne 0 ]; then            # Se o último comando não retornar 0
        zenity --error --text " $1 " # Exibe a mensagem de erro
        exit 1                       # Sai do script
    fi
}

function success() {                # $1 = Mensagem de sucesso
    if [ $? -eq 0 ]; then           # Se o último comando retornar 0
        zenity --info --text " $1 " # Exibe a mensagem de sucesso
    fi
}

function viewPDF() { # $1 = Arquivo PDF
    # Visualizar arquivo PDF (exemplo: evince arquivo.pdf)
    evince --preview $1 &>/dev/null & # Visualizar arquivo PDF
}

function dependencies() { # $1 = Nome do pacote
    # Verificar se o pacote está instalado
    if [ $(which $1) ]; then              # Se o pacote estiver instalado
        echo " Pacote $1 instalado. "     # Exibe mensagem
    else                                  # Se o pacote não estiver instalado
        echo " Pacote $1 não instalado. " # Exibe mensagem
        exit 1                            # Sai do script
    fi
}

# Dependências ----------------------------------------------------------------

dependencies zenity # Verificar se o pacote zenity está instalado
dependencies pdftk  # Verificar se o pacote pdftk está instalado
dependencies evince # Verificar se o pacote evince está instalado

# Menu Principal --------------------------------------------------------------
menu

# Fim ------------------------------------------------------------------------
